import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { getPicturesApi } from '../api/pictureApi';
import { AppDispatch, cardStore, RootState } from './store';

export const cardStatusEnum = {
    INVISIBLE: 'INVISIBLE',
    VISIBLE: 'VISIBLE'
};

export const isPairEnum = {
    PENDING: 'PENDING',
    GOOD: 'GOOD',
    BAD: 'BAD'
};

type CardSliceStateType = {
    firstCard: number | null;
    secondCard: number | null;
    isPair: string;
    cardListStatus: Array<string>;
    cardListUrl: Array<string>;
}

export const cardSLice = createSlice({
    name: 'card',
    initialState: {
        firstCard: null,
        secondCard: null,
        isPair: isPairEnum.PENDING,
        cardListStatus: [],
        cardListUrl: []
    } as CardSliceStateType,
    reducers: {
        showCard: (state: CardSliceStateType, action: PayloadAction<number>) => {
            if(state.firstCard !== null && state.secondCard != null) {
                return;
            }
            if (state.firstCard === null) {
                state.firstCard = action.payload;
            } else {
                state.secondCard = action.payload;
            }
            state.cardListStatus[action.payload] = cardStatusEnum.VISIBLE;
            if(state.firstCard !== null && state.secondCard != null) {
                if (state.cardListUrl[state.firstCard] === state.cardListUrl[state.secondCard]) {
                    state.isPair = isPairEnum.GOOD;
                } else {
                    state.isPair = isPairEnum.BAD;
                }
            }
        },
        checkPair: (state: CardSliceStateType) => {
            if(state.firstCard !== null && state.secondCard != null) {
                if(state.isPair === isPairEnum.BAD) {
                    state.cardListStatus[state.firstCard] = cardStatusEnum.INVISIBLE;
                    state.cardListStatus[state.secondCard] = cardStatusEnum.INVISIBLE;
                }
                state.firstCard = null;
                state.secondCard = null;
                state.isPair = isPairEnum.PENDING;   
            }
        },
        initCardListStatus: (state: CardSliceStateType, action: PayloadAction<number>) => {
            state.cardListStatus = Array.from(Array(action.payload)).map(() => (
                cardStatusEnum.INVISIBLE
            ));
        },
        setCardListUrl: (state: CardSliceStateType, action: PayloadAction<Array<string>>) => {
            state.cardListUrl = action.payload;
        }
    }
});

export const getPictures = (nbPictures: number) => (dispatch: AppDispatch) => {
    getPicturesApi(nbPictures).then((response) => {
        const shufflePictures: Array<string> = [...response.data, ...response.data]
            .map((picture) => (picture.url))
            .sort(() => 0.5 - Math.random());
        dispatch(cardSLice.actions.setCardListUrl(shufflePictures));
    });
};

export const { showCard, checkPair, initCardListStatus } = cardSLice.actions;

export const selectCardListStatus = (state: RootState) => (
    state[cardStore].cardListStatus
) as Array<string>;

export const selectCardListUrl = (state: RootState) => (
    state[cardStore].cardListUrl
) as Array<string>;

export const selectIsPair = (state: RootState) => (
    state[cardStore].isPair
) as string;

export default cardSLice.reducer;
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState, userStore } from './store';

type UserType = {
    name: string
}

type UserStatistics = {
    name: string,
    score: number
}

type UserSliceStateType = {
    currentUser: UserType | null,
    bestUser: UserStatistics | null
}

export const userSlice = createSlice({
    name: 'user',
    initialState: {
        currentUser: null,
        bestUser: null
    } as UserSliceStateType,
    reducers: {
        setCurrentUser: (state, action: PayloadAction<UserType>) => {
            state.currentUser = action.payload;
        },
        sendScore: (state, action: PayloadAction<number>) => {
            if (state.currentUser &&
                (!state.bestUser || state.bestUser.score <= action.payload)
            ) {
                state.bestUser = {
                    name: state.currentUser.name,
                    score: action.payload
                };
            }
        }
    }
});

export const { setCurrentUser, sendScore } = userSlice.actions;

export const selectCurrentUser = (state: RootState) => (
    state[userStore].currentUser
) as UserType;

export const selectBestUser = (state: RootState) => (
    state[userStore].bestUser
) as UserStatistics;

export default userSlice.reducer;

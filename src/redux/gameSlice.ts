import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { gameStore, RootState } from './store';

export const gameStatusEnum = {
    PENDING: 'PENDING',
    PLAYING: 'PLAYING',
    FINISH: 'FINISH'
};

type GameSliceStateType = {
    boardMemoryLength: number;
    gameStatus: string;
    pairFound: number;
}

export const gameSlice = createSlice({
    name: 'game',
    initialState: {
        boardMemoryLength: 0,
        gameStatus: gameStatusEnum.PENDING,
        pairFound: 0
    } as GameSliceStateType,
    reducers: {
        setBoardMemoryLength: (state: GameSliceStateType, action: PayloadAction<number>) => {
            state.boardMemoryLength = action.payload;
        },
        setGameStatus: (state: GameSliceStateType, action: PayloadAction<string>) => {
            state.gameStatus = action.payload;
        },
        incrPairFound: (state: GameSliceStateType) => {
            state.pairFound = state.pairFound + 1;
            if(state.pairFound === Math.pow(state.boardMemoryLength, 2) / 2) {
                state.gameStatus = gameStatusEnum.FINISH;
            }
        },
        resetPairFound: (state: GameSliceStateType) => {
            state.pairFound = 0;
        }
    }
});

export const { setBoardMemoryLength, setGameStatus, incrPairFound, resetPairFound } = gameSlice.actions;

export const selectBoardMemoryLength = (state: RootState) => (
    state[gameStore].boardMemoryLength
) as number;

export const selectGameStatus = (state: RootState) => (
    state[gameStore].gameStatus
) as string;

export const selectPairFound = (state: RootState) => (
    state[gameStore].pairFound
) as number;

export default gameSlice.reducer;

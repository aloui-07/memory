import { configureStore } from '@reduxjs/toolkit';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';

import userReducer from './userSlice';
import cardReducer from './cardSlice';
import gameReducer from './gameSlice';

export const pictureStore = 'pictureStore';
export const userStore = 'userStore';
export const gameStore = 'gameStore';
export const cardStore = 'cardStore';

const store = configureStore({
    reducer: {
        [userStore]: userReducer,
        [cardStore]: cardReducer,
        [gameStore]: gameReducer

    }
});

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export const useAppDispatch: () => AppDispatch = useDispatch;
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

export default store;


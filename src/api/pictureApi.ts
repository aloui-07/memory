import axios from 'axios';

const PICTURES_URL = 'https://jsonplaceholder.typicode.com/photos';

export function getPicturesApi(nbPictures: number) {
    return axios.get(`${PICTURES_URL}?_limit=${nbPictures}`);
}

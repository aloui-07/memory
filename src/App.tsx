import React from 'react';

import './App.css';
import MemoryManager from './component/MemoryManager/MemoryManager';
import MemoryStatistics from './component/MemoryStatistics/MemoryStatistics';
import MemoryHeader from './component/MemoryHeader/MemoryHeader';
import MemoryChrono from './component/MemoryChrono/MemoryChrono';

function App() {

    return (
        <div className="App">
            <div className="header">
                <MemoryHeader />
            </div>
            <div className="memory-content">
                <div className='memory-statistics'>
                    <MemoryStatistics />
                </div>
                <div className='memory-manager'>
                    <MemoryManager />
                </div>
                <div className='memory-chrono'>
                    <MemoryChrono />
                </div>
            </div>
        </div>
    );
}

export default App;

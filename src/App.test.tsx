import React from 'react';

import App from './App';

import { render } from '@testing-library/react';

import { Provider } from 'react-redux';
import store from './redux/store';
import { gameStatusEnum, setBoardMemoryLength, setGameStatus } from './redux/gameSlice';

const BOX_CLASSNAME = 'MuiBox-root';
const BOARD_MEMORY_LENGTH = 4;

const myRender = (children: JSX.Element) => {
    store.dispatch(setBoardMemoryLength(BOARD_MEMORY_LENGTH));
    store.dispatch(setGameStatus(gameStatusEnum.PLAYING));
    return render(<Provider store={store}>
        {children}
    </Provider>);
};

describe('App', () => {

    test('should render App with BOARD_MEMORY_LENGTH² boxes', () => {
        const { container } = myRender(<App />);
        const boxes = container.getElementsByClassName(BOX_CLASSNAME);
        expect(boxes).toHaveLength(Math.pow(BOARD_MEMORY_LENGTH, 2));
    });
});

import React from 'react';

import { Typography } from '@mui/material';

import { useAppSelector } from '../../redux/store';
import { selectBestUser } from '../../redux/userSlice';

const MemoryStatistics: React.FC = () => {

    const bestUser = useAppSelector(selectBestUser);

    return (bestUser
        ? <Typography variant='h5'>
            Le champion est {bestUser.name} avec un score de {bestUser.score}/100
        </Typography>
        : null
    );
};

export default MemoryStatistics;

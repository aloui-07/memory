import React, { useCallback, useEffect, useState } from 'react';

import BoardMemory from '../BoardMemory/BoardMemory';

import { useAppDispatch, useAppSelector } from '../../redux/store';
import {
    gameStatusEnum,
    incrPairFound,
    resetPairFound,
    selectBoardMemoryLength,
    selectGameStatus,
    selectPairFound,
    setGameStatus
} from '../../redux/gameSlice';
import {
    checkPair,
    getPictures,
    initCardListStatus,
    isPairEnum,
    selectIsPair
} from '../../redux/cardSlice';
import MemoryFinishDialog from '../MemoryFinishDialog/MemoryFinishDialog';
import { sendScore } from '../../redux/userSlice';

const MemoryManager: React.FC = () => {

    const dispatch = useAppDispatch();

    const isPair = useAppSelector(selectIsPair);

    const boardMemoryLength = useAppSelector(selectBoardMemoryLength);
    const gameStatus = useAppSelector(selectGameStatus);
    const pairFound = useAppSelector(selectPairFound);

    const [currentUserScore, setCurrentUserScore] = useState<number>(0);

    const initMemoryGame = useCallback(() => {
        const nbBoxes = Math.pow(boardMemoryLength, 2);
        dispatch(getPictures(nbBoxes / 2));
        dispatch(initCardListStatus(nbBoxes));
        dispatch(resetPairFound());
    }, [boardMemoryLength]);

    useEffect(() => {
        if(gameStatus === gameStatusEnum.PLAYING) {
            initMemoryGame();
        }
    }, [gameStatus, boardMemoryLength]);

    useEffect(() => {
        if(gameStatus === gameStatusEnum.FINISH) {
            const score = pairFound * 100 / (Math.pow(boardMemoryLength, 2) / 2);
            setCurrentUserScore(score);
            dispatch(sendScore(score));
        }
    }, [gameStatus]);

    useEffect(() => {
        if (isPair != isPairEnum.PENDING) {
            if (isPair === isPairEnum.GOOD) {
                dispatch(incrPairFound());
            }
            setTimeout(() => {
                dispatch(checkPair());
            }, 1000);
        }
    }, [isPair]);

    const handleClose = useCallback(() => {
        dispatch(setGameStatus(gameStatusEnum.PENDING));
    }, []);

    const handleValidate = useCallback(() => {
        dispatch(setGameStatus(gameStatusEnum.PLAYING));
    }, []);

    return (
        gameStatus === gameStatusEnum.PENDING
            ? null
            : <>
                <BoardMemory boardMemoryLength={boardMemoryLength} />
                <MemoryFinishDialog
                    isOpen={gameStatus === gameStatusEnum.FINISH}
                    onClose={handleClose}
                    onValidate={handleValidate}
                    score={currentUserScore}

                />
            </>
    );
};

export default MemoryManager;

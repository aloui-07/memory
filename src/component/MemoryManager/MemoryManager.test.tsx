import React from 'react';
import axios from 'axios';

import MemoryManager from './MemoryManager';

import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { Provider } from 'react-redux';
import store from '../../redux/store';
import { gameStatusEnum, setBoardMemoryLength, setGameStatus } from '../../redux/gameSlice';

import { mockCardListUrlResponse } from '../../mock/cardMock';

jest.mock('axios');

const mockedAxios = axios as jest.Mocked<typeof axios>;

const BOX_CLASSNAME = 'MuiBox-root';
const BOARD_MEMORY_LENGTH = 4;

const myRender = (children: JSX.Element) => {
    store.dispatch(setBoardMemoryLength(BOARD_MEMORY_LENGTH));
    store.dispatch(setGameStatus(gameStatusEnum.PLAYING));
    return render(<Provider store={store}>
        {children}
    </Provider>);
};

describe('MemoryManager', () => {

    afterEach(() => {
        jest.clearAllMocks();
    });

    test('should render MemoryManager with BOARD_MEMORY_LENGTH² boxes', async() => {

        mockedAxios.get.mockResolvedValue(mockCardListUrlResponse);

        const { container } = myRender(<MemoryManager />);

        await waitFor(() => {
            const boxes = container.getElementsByClassName(BOX_CLASSNAME);
            expect(boxes).toHaveLength(Math.pow(BOARD_MEMORY_LENGTH, 2));
        });
    });

    test('should display two images when clicking on two boxes', async () => {

        mockedAxios.get.mockResolvedValue(mockCardListUrlResponse);

        const { container } = myRender(<MemoryManager />);

        await waitFor(() => {
            const boxes = container.getElementsByClassName(BOX_CLASSNAME);
            userEvent.click(boxes[0]);
            userEvent.click(boxes[1]);
            expect(screen.getAllByRole('img')).toHaveLength(2);
        });
    });
});
import React, { useEffect, useState } from 'react';

import { LinearProgress } from '@mui/material';

import { gameStatusEnum, selectGameStatus, setGameStatus } from '../../redux/gameSlice';
import { useAppDispatch, useAppSelector } from '../../redux/store';

const TIME_OUT = 60;

const MemoryChronoStyle = {
    progressBar: {
        height: '10px',
        width: '50%'
    }
};

const MemoryChrono: React.FC = () => {

    const dispatch = useAppDispatch();

    const gameStatus = useAppSelector(selectGameStatus);

    const [timer, setTimer] = useState<NodeJS.Timer>();
    const [time, setTime] = useState<number>(0);

    useEffect(() => {
        switch (gameStatus) {
        case gameStatusEnum.PLAYING:
            startChrono();
            break;
        case gameStatusEnum.FINISH:
            stopChrono();
            break;
        default:
            break;
        }
    }, [gameStatus]);

    useEffect(() => {
        if (time >= TIME_OUT) {
            dispatch(setGameStatus(gameStatusEnum.FINISH));
        }
    }, [time]);

    const tick = () => {
        setTime((state) => state + 1);
    };

    const startChrono = () => {
        setTimer(setInterval(tick, 1000));
    };

    const stopChrono = () => {
        clearInterval(timer);
        setTimer(undefined);
        setTime(0);
    };

    return (
        gameStatus === gameStatusEnum.PLAYING
            ? <LinearProgress
                sx={MemoryChronoStyle.progressBar}
                value={time * 100 / TIME_OUT}
                variant="determinate"
            />
            : null
    );
};

export default MemoryChrono;

import React from 'react';

import './MemoryFinishDialog.css';

import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle
} from '@mui/material';

interface MemoryFinishDialogPropsType {
    isOpen: boolean,
    onClose: () => void
    onValidate: () => void,
    score: number
}

const MemoryFinishDialog: React.FC<MemoryFinishDialogPropsType> = (props: MemoryFinishDialogPropsType) => {
    return (
        <Dialog
            open={props.isOpen}
            onClose={props.onClose}
            maxWidth="md"
        >
            <DialogTitle>
                Résultat final
            </DialogTitle>
            <DialogContent dividers>
                <div className="finish-game-content">
                    <span>Votre score est de: {props.score}/100</span>
                    <span>Voulez vous rejouer ?</span>
                </div>
            </DialogContent>
            <DialogActions>
                <Button onClick={props.onClose}>
                    NON
                </Button>
                <Button onClick={props.onValidate}>
                    OUI
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default MemoryFinishDialog;

import React, { ChangeEvent, SyntheticEvent, useCallback, useState } from 'react';

import './MemoryHeader.css';

import PlayCircleIcon from '@mui/icons-material/PlayCircle';
import {
    Autocomplete,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    IconButton,
    TextField,
    Typography
} from '@mui/material';

import { useAppDispatch, useAppSelector } from '../../redux/store';
import { setCurrentUser } from '../../redux/userSlice';
import {
    gameStatusEnum,
    selectGameStatus,
    setBoardMemoryLength,
    setGameStatus
} from '../../redux/gameSlice';

const memoryLengthEnum = {
    SIZE_SMALL: 2,
    SIZE_MEDIUM: 4,
    SIZE_LARGE: 6
};

const memoryLengthOptions: Array<number> = [
    memoryLengthEnum.SIZE_SMALL,
    memoryLengthEnum.SIZE_MEDIUM,
    memoryLengthEnum.SIZE_LARGE
];

interface MemoryFormType {
    userName: string;
    memoryLength: number;
}

const emptyMemoryForm: MemoryFormType = {
    userName: '',
    memoryLength: memoryLengthEnum.SIZE_MEDIUM
};

const MemoryHeader: React.FC = () => {

    const [isOpen, setIsOpen] = useState(false);

    const gameStatus = useAppSelector(selectGameStatus);

    const handleClickPlay = useCallback(() => {
        setIsOpen(true);
    }, []);

    const handleClose = useCallback(() => {
        setIsOpen(false);
    }, []);

    return (
        <div className="memory-header">
            <Typography variant='h5'>
                Lancer une partie de Memory
            </Typography>
            <IconButton
                aria-label="bouton play memory"
                color="primary"
                disabled={gameStatus != gameStatusEnum.PENDING}
                onClick={handleClickPlay}
            >
                <PlayCircleIcon fontSize="large" />
            </IconButton>
            <PlayMemoryDialog
                isOpen={isOpen}
                onClose={handleClose}
            />
        </div>
    );
};

export default MemoryHeader;

interface PlayMemoryDialogPropsType {
    isOpen: boolean,
    onClose: () => void
}

const PlayMemoryDialog: React.FC<PlayMemoryDialogPropsType> = (props: PlayMemoryDialogPropsType) => {

    const dispatch = useAppDispatch();

    const [playMemoryForm, setPlayMemoryForm] = useState<MemoryFormType>(emptyMemoryForm);

    const resetMemoryForm = useCallback(() => {
        setPlayMemoryForm(emptyMemoryForm);
    }, []);

    const handleClose = useCallback(() => {
        resetMemoryForm();
        props.onClose();
    }, [props.onClose]);

    const handleValidate = useCallback(() => {
        dispatch(setCurrentUser({ name: playMemoryForm.userName }));
        dispatch(setBoardMemoryLength(playMemoryForm.memoryLength));
        dispatch(setGameStatus(gameStatusEnum.PLAYING));
        handleClose();
    }, [playMemoryForm]);

    const handleChangeUserName = useCallback((event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setPlayMemoryForm((state) => ({
            ...state,
            userName: event.target.value
        }));
    }, []);

    const handleChangeMemoryLengthSelected = useCallback((event: SyntheticEvent<Element, Event>, newValue: number | null) => {
        setPlayMemoryForm((state) => ({
            ...state,
            memoryLength: newValue || 0
        }));
    }, []);

    const isValidForm = useCallback(() => (
        playMemoryForm.userName &&
        playMemoryForm.memoryLength
    ), [playMemoryForm]);

    return (
        <Dialog
            open={props.isOpen}
            onClose={handleClose}
            maxWidth="md"
        >
            <DialogTitle>
                Jouer une partie de Memory
            </DialogTitle>
            <DialogContent dividers>
                <TextField
                    label="Entrer votre nom"
                    margin='normal'
                    required
                    autoFocus
                    fullWidth
                    value={playMemoryForm.userName}
                    onChange={handleChangeUserName}
                />
                <Autocomplete
                    noOptionsText="Aucun Résultat"
                    options={memoryLengthOptions}
                    filterSelectedOptions
                    getOptionLabel={(option) => (`${String(option)}x${String(option)}`)}
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            label="Taille du plateau"
                            margin='normal'
                        />
                    )}
                    value={playMemoryForm.memoryLength}
                    onChange={handleChangeMemoryLengthSelected}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>
                    Annuler
                </Button>
                <Button disabled={!isValidForm()} onClick={handleValidate}>
                    Valider
                </Button>
            </DialogActions>
        </Dialog>
    );
};

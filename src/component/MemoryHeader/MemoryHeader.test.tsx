import React from 'react';

import MemoryHeader from './MemoryHeader';

import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { Provider } from 'react-redux';
import store from '../../redux/store';
import { selectCurrentUser } from '../../redux/userSlice';
import { selectBoardMemoryLength } from '../../redux/gameSlice';

const TEXT_BUTTON_PLAY = 'bouton play memory';
const TEXT_BUTTON_VALIDATE = 'Valider';
const TEXT_INPUT_USER_NAME = 'Entrer votre nom';

const currentUser = {
    name: 'Walid'
};

const boardMemoryLength = 4;

const myRender = (children: JSX.Element) => {
    return render(<Provider store={store}>
        {children}
    </Provider>);
};

describe('MemoryHeader', () => {

    test('should play memory game with username user.name and board length 4x4', () => {

        myRender(<MemoryHeader />);

        const buttonPlay = screen.getByRole('button', { name: TEXT_BUTTON_PLAY });
        userEvent.click(buttonPlay);

        const inputUserName = screen.getByRole('textbox', { name: TEXT_INPUT_USER_NAME });
        userEvent.type(inputUserName, currentUser.name);

        const buttonValidate = screen.getByRole('button', { name: TEXT_BUTTON_VALIDATE });
        userEvent.click(buttonValidate);

        expect(selectCurrentUser(store.getState())).toEqual(currentUser);
        expect(selectBoardMemoryLength(store.getState())).toBe(boardMemoryLength);
    });
});
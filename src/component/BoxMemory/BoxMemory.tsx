import React, { useCallback } from 'react';

import './BoxMemory.css';

import { Box } from '@mui/material';

import { useAppDispatch, useAppSelector } from '../../redux/store';
import {
    cardStatusEnum,
    selectCardListStatus,
    selectCardListUrl,
    showCard
} from '../../redux/cardSlice';

const BoxMemoryStyle = {
    box: {
        width: '80%',
        height: '80%',
        backgroundColor: 'black',
        '&:hover': {
            backgroundColor: 'primary.main',
            opacity: [0.9, 0.8, 0.7],
        }
    }
};

export interface BoxMemoryPropsType {
    index: number
}

const BoxMemory: React.FC<BoxMemoryPropsType> = (props: BoxMemoryPropsType) => {

    const dispatch = useAppDispatch();

    const cardListUrl = useAppSelector(selectCardListUrl);
    const cardListStatus = useAppSelector(selectCardListStatus);

    const handleClickBox = useCallback(() => {
        dispatch(showCard(props.index));
    }, [props.index]);

    switch (cardListStatus[props.index]) {
    case cardStatusEnum.INVISIBLE:
        return <Box
            sx={BoxMemoryStyle.box}
            onClick={handleClickBox}
        />;
    case cardStatusEnum.VISIBLE:
        return <img
            className='box-img-rotation'
            height="80%"
            width="80%"
            src={cardListUrl[props.index]}
            alt=""
        />;
    default:
        return null;
    }
};

export default BoxMemory;

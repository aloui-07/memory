import React from 'react';

import BoxMemory, { BoxMemoryPropsType } from './BoxMemory';

import { render } from '@testing-library/react';

import { Provider } from 'react-redux';
import store from '../../redux/store';
import { initCardListStatus } from '../../redux/cardSlice';

const BOX_CLASSNAME = 'MuiBox-root';
const BOARD_MEMORY_LENGTH = 4;

const myRender = (children: JSX.Element) => {
    store.dispatch(initCardListStatus(Math.pow(BOARD_MEMORY_LENGTH, 2)));
    return render(<Provider store={store}>
        {children}
    </Provider>);
};

const props: BoxMemoryPropsType = {
    index: 0
};

describe('BoxMemory', () => {

    test('should render one box', () => {
        const { container } = myRender(<BoxMemory {...props} />);
        const boxes = container.getElementsByClassName(BOX_CLASSNAME);
        expect(boxes).toHaveLength(1);
    });
});

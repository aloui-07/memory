import React from 'react';

import BoardMemory, { BoardMemoryPropsType } from './BoardMemory';

import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { Provider } from 'react-redux';
import store from '../../redux/store';
import { initCardListStatus } from '../../redux/cardSlice';

const BOX_CLASSNAME = 'MuiBox-root';

const myRender = (children: JSX.Element) => {
    store.dispatch(initCardListStatus(Math.pow(props.boardMemoryLength, 2)));
    return render(<Provider store={store}>
        {children}
    </Provider>);
};

const props: BoardMemoryPropsType = {
    boardMemoryLength: 4
};

describe('BoardMemory', () => {

    test('should render BoardMemory with boardMemoryLength² boxes', () => {
        const { container } = myRender(<BoardMemory {...props} />);
        const boxes = container.getElementsByClassName(BOX_CLASSNAME);
        expect(boxes).toHaveLength(Math.pow(props.boardMemoryLength, 2));
    });

    test('should display two images when clicking on two boxes', () => {
        const { container } = myRender(<BoardMemory {...props} />);
        const boxes = container.getElementsByClassName(BOX_CLASSNAME);
        userEvent.click(boxes[0]);
        userEvent.click(boxes[1]);
        expect(screen.getAllByRole('img')).toHaveLength(2);
    });
});
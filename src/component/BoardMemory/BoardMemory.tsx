import React from 'react';

import BoxMemory from '../BoxMemory/BoxMemory';

import { Grid, Paper } from '@mui/material';

const BoardMemoryStyle = {
    paper: {
        backgroundColor: 'darkgray',
        border: '1px solid black',
        height: '100%',
        width: '100%',
        userSelect: 'none'
    }
};

interface BoardMemoryColumnPropsType {
    index: number;
}

const BoardMemoryColumn: React.FC<BoardMemoryColumnPropsType> = (props: BoardMemoryColumnPropsType) => {

    return (
        <Grid
            item
            xs={1}
            display="flex"
            alignItems="center"
            justifyContent="center"
            height="80%"
        >
            <BoxMemory index={props.index} />
        </Grid>
    );
};

interface BoardMemoryRowPropsType {
    rowIndex: number;
    boardMemoryLength: number
}

const BoardMemoryRow: React.FC<BoardMemoryRowPropsType> = (props: BoardMemoryRowPropsType) => {

    return (
        <Grid
            container
            alignItems="center"
            height={`${100 / props.boardMemoryLength}%`}
            columns={props.boardMemoryLength}
        >
            {Array.from(Array(props.boardMemoryLength)).map((_, index) => (
                <BoardMemoryColumn
                    key={index}
                    index={props.rowIndex * props.boardMemoryLength + index}
                />
            ))}
        </Grid>
    );
};

export interface BoardMemoryPropsType {
    boardMemoryLength: number;
}

const BoardMemory: React.FC<BoardMemoryPropsType> = (props: BoardMemoryPropsType) => {

    return (
        <Paper
            sx={BoardMemoryStyle.paper}
            square
        >
            <Grid
                container
                direction="column"
                height="100%"
            >
                {Array.from(Array(props.boardMemoryLength)).map((_, index) => (
                    <BoardMemoryRow
                        key={index}
                        rowIndex={index}
                        boardMemoryLength={props.boardMemoryLength}
                    />
                ))}
            </Grid>
        </Paper>
    );
};

export default BoardMemory;
